package com.example.nataniel.lab4;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import static android.content.ContentValues.TAG;

public class A3 extends AppCompatActivity
{
    private ListView messagesFromUser;
    private FirebaseFirestore db;
    private String chosenUsername;
    private ArrayList<messageResults> messageResultsFromUser;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a3);

        db = FirebaseFirestore.getInstance();
        messagesFromUser = findViewById(R.id.listviewMessagesFromUser);
        messageResultsFromUser = getMessagesFromUser();

        Intent intent = getIntent();
        chosenUsername = intent.getStringExtra(TwoFragment.USERNAME_CHOSEN);
    }

    private ArrayList<messageResults> getMessagesFromUser()
    {
        final ArrayList<messageResults> resultsForUser = new ArrayList<messageResults>();

        db.collection("messages").addSnapshotListener(new EventListener<QuerySnapshot>()
        {
            @Override
            public void onEvent(@Nullable QuerySnapshot value,
                                @Nullable FirebaseFirestoreException e)
            {
                if (e != null)
                {
                    Log.w(TAG, "Listen failed.", e);
                    return;
                }

                for (DocumentSnapshot doc : value)
                {
                    String check = doc.get("u").toString();
                    if (check.equals(chosenUsername))
                    {
                        messageResults messageResultsFromUser = new messageResults();
                        messageResultsFromUser.setDate(doc.get("d").toString());
                        messageResultsFromUser.setUser(doc.get("u").toString());
                        messageResultsFromUser.setMessage(doc.get("m").toString());
                       // Log.d(TAG, "--------------------Current messages in database: " + messageResultsFromUser.getMessage());
                        resultsForUser.add(messageResultsFromUser);
                    }
                }

                //sort list by date //TODO make a better 'sort by date' function
                Collections.sort(resultsForUser, new Comparator<messageResults>()
                {
                    public int compare(messageResults o1, messageResults o2)
                    {
                        if (o1.getDate() == null || o2.getDate() == null)
                            return 0;
                        return o2.getDate().compareTo(o1.getDate());
                    }
                });
                messagesFromUser.setAdapter(new customBaseAdapter2(A3.this, messageResultsFromUser));
            }
        });
        return resultsForUser;
    }
}
