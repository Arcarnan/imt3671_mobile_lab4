package com.example.nataniel.lab4;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import static android.content.ContentValues.TAG;


public class TwoFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener
{
    ListView listviewUsernames;
    ArrayList<String> usernames;
    public static final String USERNAME_CHOSEN = "usernameDefaultTwoFragment"; //  username chosen when clicking on a user in A1, TwoFragment
    public SwipeRefreshLayout mSwipeLayout;
    private FirebaseFirestore db;

    public TwoFragment()
    {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        final View rootViewFragmentTwo = inflater.inflate(R.layout.fragment_two, container, false);

        listviewUsernames = rootViewFragmentTwo.findViewById(R.id.listViewContacts);
        db = FirebaseFirestore.getInstance();

        refreshUserList();

        //  go to new viewer when clicking an item in the listView
        listviewUsernames.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                String username = usernames.get(position);
                Intent intent = new Intent (getActivity(), A3.class);
                String message = username;
                intent.putExtra(USERNAME_CHOSEN, message);
                startActivity (intent);
            }
        });

        //  pull-down refresh to reload the feed
        mSwipeLayout = (SwipeRefreshLayout) rootViewFragmentTwo.findViewById(R.id.swipeRefreshLayout);
        mSwipeLayout.setOnRefreshListener(this);

        // Inflate the layout for this fragment
        return rootViewFragmentTwo;
    }

    @Override
    public void onRefresh()
    {
        mSwipeLayout.setColorScheme(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        refreshUserList();
        mSwipeLayout.setRefreshing(false);
    }


    public void refreshUserList()
    {
        db.collection("users")
                .addSnapshotListener(new EventListener<QuerySnapshot>()
                {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot value,
                                        @Nullable FirebaseFirestoreException e)
                    {
                        if (e != null)
                        {
                            Log.w(TAG, "Listen failed.", e);
                            return;
                        }

                        usernames = new ArrayList<String>();
                        for (DocumentSnapshot doc : value)
                        {
                            if (doc.get("username") != null)
                            {
                                usernames.add(doc.getString("username"));
                            }
                        }

                        //sort usernames in alphabetical order
                        Collections.sort(usernames, new Comparator<String>()
                        {
                            @Override
                            public int compare(String s1, String s2)
                            {
                                return s1.compareToIgnoreCase(s2);
                            }
                        });

                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, usernames);
                        listviewUsernames.setAdapter(adapter);

                        Log.d(TAG, "--------------------Current users in database: " + usernames);


                      //if no elements in usernames, show TOAST message
                      if (usernames == null)
                      {
                           Toast.makeText(getActivity(), "No users in the database. ",
                                 Toast.LENGTH_LONG).show();
                      }
                    }
                });
    }
}