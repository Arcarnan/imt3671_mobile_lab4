package com.example.nataniel.lab4;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class customBaseAdapter2 extends BaseAdapter
{
    private static ArrayList<messageResults> searchArrayList;

    private LayoutInflater mInflater;

    public customBaseAdapter2(Context context, ArrayList<messageResults> results)
    {
        searchArrayList = results;
        mInflater = LayoutInflater.from(context);
    }

    public int getCount()
    {
        return searchArrayList.size();
    }

    public Object getItem(int position)
    {
        return searchArrayList.get(position);
    }

    public long getItemId(int position)
    {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent)
    {
        ViewHolder holder;
        if (convertView == null)
        {
            convertView = mInflater.inflate(R.layout.messages_listview_item, null);
            holder = new ViewHolder();
            holder.txtDate = (TextView) convertView.findViewById(R.id.dateListviewText);
            holder.txtUser = (TextView) convertView.findViewById(R.id.usernameListviewText);
            holder.txtMessage = (TextView) convertView.findViewById(R.id.messageListviewText);

            convertView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.txtDate.setText(searchArrayList.get(position).getDate());
        holder.txtUser.setText(searchArrayList.get(position).getUser());
        holder.txtMessage.setText(searchArrayList.get(position).getMessage());
        notifyDataSetChanged();

        return convertView;
    }

    static class ViewHolder
    {
        TextView txtDate;
        TextView txtUser;
        TextView txtMessage;
    }
}
