package com.example.nataniel.lab4;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class A2 extends AppCompatActivity
{

    public Button mApplyButton;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a2);

        // For dropdown menu:
        final Spinner spinnerRefreshRate = findViewById(R.id.spinnerRefreshRate);
        final TextView usernameDisplay = findViewById(R.id.textViewUsernameDisplay);

        List<String> refreshRate = new ArrayList<>();
        refreshRate.add("10 min");
        refreshRate.add("1 hour");
        refreshRate.add("24 hours");

        //saving choices for refresh rate
        ArrayAdapter<String> dataAdapterRefreshRate = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, refreshRate);
        dataAdapterRefreshRate.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerRefreshRate.setAdapter(dataAdapterRefreshRate);

        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        final int savedRefreshRate = preferences.getInt("savedRefreshRate", -1);
        if (savedRefreshRate != -1)
        {
            spinnerRefreshRate.setSelection(savedRefreshRate);
        }

        final String savedUsername = preferences.getString("savedUsername", null);
        usernameDisplay.setText(savedUsername);



        mApplyButton = (Button) findViewById(R.id.buttonA2Apply);
        mApplyButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)         //TODO change the activity change to the actual RSS fetch
            {
                //for saving the choice of refresh rate only if "Apply" button is pressed
                final SharedPreferences refreshPreferences = PreferenceManager.getDefaultSharedPreferences(A2.this);
                final SharedPreferences.Editor refreshEditor = refreshPreferences.edit();
                final Spinner spinnerRefreshRate = findViewById(R.id.spinnerRefreshRate);
                final int savedRefreshRate = spinnerRefreshRate.getSelectedItemPosition();
                refreshEditor.putInt("savedRefreshRate", savedRefreshRate);
                refreshEditor.apply();

                startActivity(new Intent(A2.this, A1.class));
            }
        });
    }
}
