package com.example.nataniel.lab4;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static android.content.ContentValues.TAG;


public class OneFragment extends Fragment implements View.OnClickListener
{
    final String CHANNEL_ID = "Lab 4. ";
    private Button mSendButton;
    private ListView listviewMessages;
    public FirebaseFirestore db;
    private Runnable timedTask;
    private Handler mRepeatHandler;
    private boolean newMessageRecieved;
    private SharedPreferences numberOfItemsPreference;
    EditText mEdit;
    ArrayList<messageResults> messageResults;
    String messageLast;
    String userLastMessage;

    public OneFragment()
    {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View rootViewFragmentOne = inflater.inflate(R.layout.fragment_one, container, false);

        db = FirebaseFirestore.getInstance();
        listviewMessages = rootViewFragmentOne.findViewById(R.id.listViewMessages);
        messageResults = getMessageResults();
        newMessageRecieved = false;

        //refreshMessageList();
        numberOfItemsPreference = PreferenceManager.getDefaultSharedPreferences(getContext());

        mSendButton = (Button) rootViewFragmentOne.findViewById(R.id.buttonSendMessage);
        mSendButton.setOnClickListener(this);
        mEdit = (EditText) rootViewFragmentOne.findViewById(R.id.textMessage);
        mRepeatHandler = new Handler();

        //  for updating at given time intervals/ fetch data from the provided RSS feed every chosen time interval
        timedTask = new Runnable()
        {
            @Override
            public void run()
            {
                new ProcessInBackground().execute();
                mRepeatHandler.postDelayed(timedTask, getDesiredRefreshRate());
            }
        };
        mRepeatHandler.post(timedTask);

        // Inflate the layout for this fragment
        return rootViewFragmentOne;
    }


    @Override
    public void onClick(View view)
    {
        String message = mEdit.getText().toString();

        if (message.length() > 256)
        {
            Toast.makeText(getActivity(), "Message must be shorter than 256 characters. ",
                    Toast.LENGTH_LONG).show();
        }
        else
        {
            //  Try to load a saved username
            final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
            final String savedUsername = preferences.getString("savedUsername", null);

            //get current system date and time
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.UK);
            String date = sdf.format(Calendar.getInstance().getTime());

            // Add a new document with a generate65d id.
            Map<String, Object> data = new HashMap<>();
            data.put("d", date);
            data.put("u", savedUsername);
            data.put("m", message);

            db.collection("messages")
                    .add(data)
                    .addOnSuccessListener(new OnSuccessListener<DocumentReference>()
                    {
                        @Override
                        public void onSuccess(DocumentReference documentReference)
                        {
                            Log.d(TAG, "DocumentSnapshot written with ID: " + documentReference.getId());
                        }
                    })
                    .addOnFailureListener(new OnFailureListener()
                    {
                        @Override
                        public void onFailure(@NonNull Exception e)
                        {
                            Log.w(TAG, "Error adding document", e);
                        }
                    });
            mEdit.getText().clear();
            newMessageRecieved = false;
        }
    }

    private ArrayList<messageResults> getMessageResults()
    {
        final ArrayList<messageResults> results = new ArrayList<messageResults>();

        db.collection("messages").addSnapshotListener(new EventListener<QuerySnapshot>()
        {
            @Override
            public void onEvent(@Nullable QuerySnapshot value,
                                @Nullable FirebaseFirestoreException e)
            {
                if (e != null) {
                    Log.w(TAG, "Listen failed.", e);
                    return;
                }

                //  clear the old list to avoid duplicates
                results.clear();
                for (DocumentSnapshot doc : value) {
                    if (doc.get("u") != null && doc.get("m") != null && doc.get("d") != null) {
                        messageResults messageResult = new messageResults();
                        messageResult.setDate(doc.get("d").toString());
                        messageResult.setUser(doc.get("u").toString());
                        messageResult.setMessage(doc.get("m").toString());
                        //Log.d(TAG, "--------------------Current messages in database: " + messageResult.getMessage());
                        results.add(messageResult);
                    }
                }

                //sort list by date //TODO make a better 'sort by date' function
                Collections.sort(results, new Comparator<messageResults>()
                {
                    public int compare(messageResults o1, messageResults o2)
                    {
                        if (o1.getDate() == null || o2.getDate() == null)
                            return 0;
                        return o2.getDate().compareTo(o1.getDate());
                    }
                });

                listviewMessages.setAdapter(new customBaseAdapter(getContext(), messageResults));

                userLastMessage = results.get(0).getUser();
                messageLast = results.get(0).getMessage();
                newMessageRecieved = true;
            }
        });
        return results;
    }


    public void notificationSettings()
    {
        // Create an explicit intent for an Activity in your app
        Intent intent = new Intent(getContext(), A1.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(getActivity(), 0, intent, 0);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getContext());


        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getContext(), CHANNEL_ID)
                //.setStyle(new NotificationCompat.MessagingStyle("Me")
                .setSmallIcon(R.drawable.ic_tab_contacts)
                .setContentTitle(userLastMessage)  //(messageResults.get(1).getUser())
                .setContentText(messageLast) //(messageResults.get(1).getMessage())
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                // Set the intent that will fire when the user taps the notification
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);


        // notificationId is a unique int for each notification that you must define, allows you to update the notification later on.
        int notificationId = 0;
        notificationManager.notify(notificationId, mBuilder.build());

    }

    //  run the notification manager in the background
    public class ProcessInBackground extends AsyncTask<Integer, Integer, Exception>   //type accepted, incrementing progress, what is returned
    {
        Exception exception = null;

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            //TODO fill if ever relevant
        }

        @Override
        protected Exception doInBackground(Integer... integers)
        {
            //try { //  for updating at given time intervals/ fetch data from the provided RSS feed every chosen time interval
            if (newMessageRecieved)
            {
                notificationSettings();
                newMessageRecieved = false;
            }

            /*} catch (IOException e) {
                exception = e;
            }*/
            return exception;
        }

        @Override
        protected void onPostExecute(Exception s)
        {
            super.onPostExecute(s);
            //TODO fill if ever relevant
        }
    }


    public Integer getDesiredRefreshRate()
    {
        final int numberOfItemsSpinnerPosition = numberOfItemsPreference.getInt("savedRefreshRate", 1);
        int desiredNumber = 0;

        if (numberOfItemsSpinnerPosition == 0) {
            desiredNumber = 1000 * 60 * 10; //10 minutes
        } else if (numberOfItemsSpinnerPosition == 1) {
            desiredNumber = 1000 * 60 * 60; //1 hours
        } else if (numberOfItemsSpinnerPosition == 2) {
            desiredNumber = 1000 * 60 * 60 * 24;    //24 hours
        }
        return desiredNumber;
    }
}
