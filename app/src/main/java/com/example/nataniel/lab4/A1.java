package com.example.nataniel.lab4;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class A1 extends AppCompatActivity
{
    private String USER_NAME_LOCAL;     //  user name for the local device
    final private int MAX_NAME_LENGTH = 35;
    private static final String TAG = "A1";

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private int[] tabIcons = {
            R.drawable.icon_sms,
            R.drawable.ic_tab_contacts
    };
    private FirebaseFirestore db;
    private FirebaseAuth mAuth;

    public ArrayList<String> messages;

    public Button mSettingsButton;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        //  TODO Suggest a (random) nickname

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a1);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        setupToolbar();
        setupTabIcons();

        //  button for changing activity to settings
        mSettingsButton = findViewById(R.id.ButtonSettings);
        mSettingsButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                startActivity(new Intent(A1.this, A2.class));
            }
        });

        setUsername();
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
    }


    private void setupToolbar()
    {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void setupTabIcons()
    {
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
    }

    private void setupViewPager(ViewPager viewPager)
    {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new OneFragment(), "Chat");
        adapter.addFragment(new TwoFragment(), "Users");
        viewPager.setAdapter(adapter);
    }

    //User is only allowed to enter a username first time starting the app
    private void setUsername()
    {
        //  Try to load a saved username
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        USER_NAME_LOCAL = preferences.getString("savedUsername", null);

        //  if no user name is saved, AlertDialog asks the user to choose a (valid) username
        if (USER_NAME_LOCAL == null || USER_NAME_LOCAL.isEmpty())
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Please choose a username. ");

            final EditText input = new EditText(this);
            input.setInputType(InputType.TYPE_CLASS_TEXT);
            builder.setView(input);

            // Set up the buttons
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener()
            {

                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    String user = input.getText().toString();
                    if (user.isEmpty())
                    {
                        Toast.makeText(A1.this, "Username cannot be empty. ", Toast.LENGTH_LONG).show();
                        setUsername();
                    }
                    else if (user.length() > MAX_NAME_LENGTH)
                    {
                    Toast.makeText(A1.this, "Username cannot be more than " + MAX_NAME_LENGTH + " characters long. ", Toast.LENGTH_LONG).show();
                    setUsername();
                    }
                    else if (user != null && !user.isEmpty())
                    {
                        USER_NAME_LOCAL = input.getText().toString();

                        // Create a new user with a username
                        Map<String, Object> userToStore = new HashMap<>();
                        userToStore.put("username", USER_NAME_LOCAL);

                        // Add a new document with a generated ID
                        db.collection("users")
                                .add(userToStore)
                                .addOnSuccessListener(new OnSuccessListener<DocumentReference>()
                                {
                                    @Override
                                    public void onSuccess(DocumentReference documentReference)
                                    {
                                        Log.d(TAG, "DocumentSnapshot added with ID: " + documentReference.getId());
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener()
                                {
                                    @Override
                                    public void onFailure(@NonNull Exception e)
                                    {
                                        Log.w(TAG, "Error adding document", e);
                                    }
                                });
                    }
                }
            });
        builder.show();
      }
    }



    @Override
    public void onStart()
    {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);

        if (currentUser == null)
        {
            mAuth.signInAnonymously()
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>()
                    {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task)
                        {
                            if (task.isSuccessful())
                            {
                                // Sign in success, update UI with the signed-in user's information
                                Log.d(TAG, "signInAnonymously:success");
                                FirebaseUser user = mAuth.getCurrentUser();
                                updateUI(user);
                                Toast.makeText(A1.this, "Authentication success.",
                                        Toast.LENGTH_SHORT).show();
                            }

                            else
                            {
                                // If sign in fails, display a message to the user.
                                Log.w(TAG, "signInAnonymously:failure", task.getException());
                                Toast.makeText(A1.this, "Authentication failed. Check internet connectivity",
                                        Toast.LENGTH_LONG).show();
                                updateUI(null);
                            }

                            // ...
                        }
                    });
        }
    }


    private void updateUI(FirebaseUser user)
    {
        //TODO update UI if if this becomes relevant
    }


    class ViewPagerAdapter extends FragmentPagerAdapter
    {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager)
        {
            super(manager);
        }

        @Override
        public Fragment getItem(int position)
        {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title)
        {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position)
        {
            return mFragmentTitleList.get(position);
        }
    }


    //  closes app if back button is clicked from the listView activity (A1, the main activity)
    @Override
    public void onBackPressed()
    {
        new AlertDialog.Builder(this)
                .setTitle("Exit application")
                .setMessage("Are you sure you want to exit?")
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface arg0, int arg1)
                    {
                        //A1.super.onBackPressed();
                        finishAffinity();
                    }
                }).create().show();
    }


     @Override
    protected void onPause()
     {
         super.onPause();

         final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
         final SharedPreferences.Editor editor = preferences.edit();
         final String savedUsername = USER_NAME_LOCAL;
         editor.putString("savedUsername", savedUsername);
         editor.apply();
     }
}
